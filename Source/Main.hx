package;

import flash.events.Event;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;
import haxe.Timer;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.events.KeyboardEvent;
import openfl.events.TimerEvent;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import openfl.ui.Keyboard;

class Main extends Sprite {
	
	//Declarations
	
	//The interal rendering resolution of the display
	private static inline var w: Int = 320;
	private static inline var h: Int = 208;
	private var bounds: Rectangle;
	private var origin: Point;
	
	// engine does the actual thinking and construction of each frame, then copy to buffer to complete a raw framedraw.
	private var engine: Raycaster;
	private var buffer: BitmapData;
	
	// the actual element drawn on the screen, resized and whatever else turns out to be needed
	private var display: Bitmap;
	
	private var kbUP:Bool = false;
	private var kbDOWN:Bool = false;
	private var kbLEFT:Bool = false;
	private var kbRIGHT:Bool = false;
	
	private var TURNSPEED: Float =3;
	private var MOVESPEED: Float = 5;
	
	private var debugstuff:TextField;
	
	private var oldtime:Float;
	private var time:Float;


    public function new () 
	{

        super ();
		bounds = new Rectangle(0, 0, w, h);
		origin = new Point();
		
		/*var timer: Timer = new Timer(20);
		timer.addEventListener( TimerEvent.TIMER, enterFrame);
		timer.start();
		*/
		var fps:FPS = new FPS(10, 100, 0x888888);
		debugstuff = new TextField();
		debugstuff.x = 10;
		debugstuff.y = 400;
		debugstuff.defaultTextFormat = new TextFormat("_sans", 12, 0x888888);
		debugstuff.text = "debug";
		debugstuff.autoSize = TextFieldAutoSize.LEFT;
		
		engine = new Raycaster(w, h);
		buffer = new BitmapData(w,h,true, 0xAAAAAAAA);
		
		display = new Bitmap(buffer);
        display.x = (stage.stageWidth - display.width) / 2;
		display.y = (stage.stageHeight - display.height) / 2;
		
		oldtime = Timer.stamp();
		
		addChild(display);
		addChild(fps);
		addChild(debugstuff);
		
		this.addEventListener( Event.ENTER_FRAME, everyframe);
		
		//set up crappy keyboard controls
		stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
		stage.addEventListener(KeyboardEvent.KEY_UP, keyUP);
		

    }
	
	private function keyUP(e:KeyboardEvent):Void 
	{
		if (e.keyCode == Keyboard.UP) kbUP = false;
		else if (e.keyCode == Keyboard.DOWN) kbDOWN = false;
		else if (e.keyCode == Keyboard.LEFT) kbLEFT = false;
		else if (e.keyCode == Keyboard.RIGHT) kbRIGHT = false;
		
	}
	
	private function keyDown(e:KeyboardEvent):Void 
	{
		if (e.keyCode == Keyboard.UP) kbUP = true;
		else if (e.keyCode == Keyboard.DOWN) kbDOWN = true;
		else if (e.keyCode == Keyboard.LEFT) kbLEFT = true;
		else if (e.keyCode == Keyboard.RIGHT) kbRIGHT = true;
	}
	
	private function everyframe(e:Event):Void 
	{
		time = Timer.stamp();
		var frameTime : Float = (time - oldtime);
		oldtime = time;
		var movement : Float = MOVESPEED * frameTime;
		var rotation : Float = TURNSPEED * frameTime;
		
		if (kbRIGHT)
		{
			var olddirX : Float = engine.dirX;
			engine.dirX = engine.dirX * Math.cos( -rotation) - engine.dirY * Math.sin( -rotation);
			engine.dirY = olddirX * Math.sin( -rotation) + engine.dirY * Math.cos( -rotation);
			var oldcamX : Float = engine.camPlaneX;
			engine.camPlaneX = engine.camPlaneX * Math.cos( -rotation) - engine.camPlaneY * Math.sin( -rotation);
			engine.camPlaneY = oldcamX * Math.sin( -rotation) + engine.camPlaneY * Math.cos( -rotation);
		}
		if (kbLEFT)
		{
			var olddirX : Float = engine.dirX;
			engine.dirX = engine.dirX * Math.cos( rotation) - engine.dirY * Math.sin( rotation);
			engine.dirY = olddirX * Math.sin( rotation) + engine.dirY * Math.cos( rotation);
			var oldcamX : Float = engine.camPlaneX;
			engine.camPlaneX = engine.camPlaneX * Math.cos( rotation) - engine.camPlaneY * Math.sin( rotation);
			engine.camPlaneY = oldcamX * Math.sin( rotation) + engine.camPlaneY * Math.cos( rotation);
		}
		if (kbUP)
		{
			engine.posX += engine.dirX * movement;
			engine.posY += engine.dirY * movement;
		}		
		if (kbDOWN)
		{
			engine.posX -= engine.dirX * movement;
			engine.posY -= engine.dirY * movement;
		}
		
		engine.render();
		buffer.copyPixels(engine, bounds, origin);
		//debugstuff.text = engine.x + " " + engine.y + " " + engine.dir;
	}
	

}