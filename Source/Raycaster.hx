package;

import flash.geom.Point;
import flash.geom.Rectangle;
import openfl.Assets;
import openfl.display.BitmapData;

/**
 * ...
 * @author David Bell
 */
class Raycaster extends BitmapData
{

	// The camera coordinates. Should probably replace this with a custom class
	public var posX: Float;
	public var posY: Float;
	public var dirX: Float;
	public var dirY: Float;
	public var camPlaneX: Float;
	public var camPlaneY: Float;

	
	//Used to track stuff about the wall we're drawing
	private var side: Int; //if the wall is NS or EW
	private var wallOffset: Float; //Where along the wall the ray hit
	
	// Make a generic map out of an array of ints
	private var map:Array<Int>;
	private static inline var MAPHEIGHT:Int = 64;
	private static inline var MAPWIDTH:Int = 64;
	
	
	
	// Texture bitmaps
	private var ceiltex:BitmapData;
	private var walltex:BitmapData;
	private var floortex:BitmapData;
	
	// Magic numbers
	private static inline var SIGHTRANGE:Float = 60; // TODO: Lock down what this number should be
	private static inline var FOCALLENGTH: Float = .66;
	
	
	//lookup tables
	private var ctable:Array<Float>;
	private var ftable:Array<Float>;
	
	public function new(width:Int, height:Int) 
	{
		super(width, height, false, 0);
		
		// Initialize the camera just kind of wherever for now
		posX = 1.5; 
		posY = 1.5;
		dirX = -1; 
		dirY = 0;
		camPlaneX = 0;
		camPlaneY = FOCALLENGTH;
		
		//origin = new Point(x, y);
		//fov = FIELDOFVIEW * Math.PI / 180;
		
		// Load the bmps
		floortex = Assets.getBitmapData("textures/floor.png");
		ceiltex = Assets.getBitmapData("textures/ceiling.png");
		walltex = Assets.getBitmapData("textures/wall.png");
		
		
		// Initialize the map
		// TODO: replace map array with a custom map with distinct walls, floor, ceiling
		var i:Int;
		map = new Array<Int>();
		
		for( i in 0...(MAPHEIGHT * MAPWIDTH))
		{
			var wallcode : Int;
			if (i < MAPWIDTH) wallcode = 1;
			else if (i % MAPHEIGHT == 0) wallcode = 1;
			else if (i % MAPHEIGHT == MAPWIDTH - 1) wallcode = 1;
			else if (i > MAPWIDTH * (MAPHEIGHT - 1)) wallcode = 1;
			else if (Math.random() < .2) wallcode = 1;
			else wallcode = 0;
			
			map[i] = wallcode;
			
		}
		
		ctable = new Array<Float>();
		for (i in 0...height)
		{
			ctable[i] = height / (height - 2.0 * i);
		}
		
		ftable = new Array<Float>();
		for (i in 0...height)
		{
			ftable[i] = height / (2.0 * i - height);
		}
		
	}
	
	public function render() : Void
	{
		// TODO: calculate and draw all the pixels
		
		var sx: Int; // which column is being rendered
		var sy: Int; // Which pixel in the column is being rendered
		
		var rayPosX : Float = posX;
		var rayPosY : Float = posY;
		
		lock(); //performance improver
		
		for (sx in 0...width)
		{
			//Main loop goes in here
			//TODO: It might be easier to combine raycast() and drawcolumn()
			
			var cameraX : Float = 2.0 * sx / width - 1;
			var rayDirX : Float = dirX + camPlaneX * cameraX;
			var rayDirY : Float = dirY + camPlaneY * cameraX;
			
			var mapX : Int = Math.floor(rayPosX);
			var mapY : Int = Math.floor(rayPosY);		
			
			// Length of ray from current position to next x or y side
			var sideDistX : Float;
			var sideDistY : Float;
			
			//length of ray from one x or y side to next x or y side
			var deltaDistX : Float = Math.sqrt(1 + (rayDirY * rayDirY) / (rayDirX * rayDirX));
			var deltaDistY : Float = Math.sqrt(1 + (rayDirX * rayDirX) / (rayDirY * rayDirY));
			var perpWallDist : Float = Math.POSITIVE_INFINITY;
			
			//Which direction to step in x or y direction, either +1 or -1)
			var stepX, stepY : Int;
			
			var raylength : Float = 0; // only draw as far as the sight range
			//var side : Int; // was a NS or EW wall hit?
			
			if (rayDirX < 0)
			{
				stepX = -1;
				sideDistX = (rayPosX - mapX) * deltaDistX;
			}
			else
			{
				stepX = 1;
				sideDistX = (mapX + 1.0 - rayPosX) * deltaDistX;
			}
			if (rayDirY < 0)
			{
				stepY = -1;
				sideDistY = (rayPosY - mapY) * deltaDistY;
			}
			else
			{
				stepY = 1;
				sideDistY = (mapY + 1.0 - rayPosY) * deltaDistY;
			}
			
			// Look for a hit
			for (i in 0...SIGHTRANGE)
			{
				// jump to next map square, or in x-dir or in y-dir
				if (sideDistX < sideDistY)
				{
					sideDistX += deltaDistX;
					mapX += stepX;
					side = 0;
				}
				else
				{
					sideDistY += deltaDistY;
					mapY += stepY;
					side = 1;
				}
				if (isWall(mapX, mapY) > 0)
				{
					if (side == 0) perpWallDist = (mapX - rayPosX + (1 - stepX) / 2) / rayDirX;
					else           perpWallDist = (mapY - rayPosY + (1 - stepY) / 2) / rayDirY;
					break;
				}
			}
			//return perpWallDist;
			
			
			//var distance : Float = raycast(rayPosX, rayPosY, rayDirX, rayDirY);
			if (side == 0) wallOffset = rayPosY + perpWallDist * rayDirY;
			else           wallOffset = rayPosX + perpWallDist * rayDirX;
			wallOffset -= Math.floor(wallOffset);
			
			var texX : Int = Math.floor(wallOffset * walltex.width);
			//these are to fix mirroring issues
			if (side == 0 && rayDirX > 0) texX = walltex.width - texX - 1;
			if (side == 1 && rayDirY < 0) texX = walltex.width - texX - 1;
			
			
			//work out bottom and top of wall. Symmetric, so only need to do half the math here
			//var z: Float = distance * Math.abs(Math.cos(angle));
			var wallheight: Float = height / perpWallDist;  // For now, just assume all walls are the same height
			var top:Float = (height - wallheight) / 2;
			var bottom:Float = height - ((height -wallheight) / 2);
			
			//Floors and ceilings
			var floorXWall : Float;
			var floorYWall : Float;
			
			//4 different cases depending on which wall we're drawing out to
			if (side == 0 && rayDirX > 0)
			{
				floorXWall = mapX;
				floorYWall = mapY + wallOffset;
			}
			else if (side == 0 && rayDirX < 0)
			{
				floorXWall = mapX + 1.0;
				floorYWall = mapY + wallOffset;
			}
			else if (side == 1 && rayDirY > 0)
			{
				floorXWall = mapX + wallOffset;
				floorYWall = mapY;
			}
			else
			{
				floorXWall = mapX + wallOffset;
				floorYWall = mapY + 1.0;
			}
			
			
			
			for (sy in 0...height)
			{
				//If sy < the top, draw a ceiling pixel
				if (sy < top) 
				{
					setPixel(sx, sy, 0x00FFFF);
					var d: Float = ctable[sy];
					var weight: Float = d / perpWallDist;
					var currentFloorX: Float = weight * floorXWall + (1.0 - weight) * posX;
					var currentFloorY: Float = weight * floorYWall + (1.0 - weight) * posY;
					var TexX: Int = Math.floor(currentFloorX * ceiltex.width) % ceiltex.width;
					var TexY: Int = Math.floor(currentFloorY * ceiltex.height) % ceiltex.height;
					var pix: Int = ceiltex.getPixel(TexX, TexY);
					setPixel(sx, sy, pix);
					
				}
				//if top <= sy <= bottom draw a wall pixel
				else if (sy < bottom) 
				{
					var d: Float = 2 * sy - height + wallheight;
					var texY: Int = Math.floor((d * walltex.height) / wallheight/2);
					var pix: Int = walltex.getPixel(texX, texY);
					if (side == 1) pix = (pix >> 1) & 8355711;
					setPixel(sx, sy, Math.floor(pix));
				}
				//else draw a floor pixel
				else 
				{
					setPixel(sx, sy, 0x808000);
					var d: Float = ftable[sy];
					var weight: Float = d / perpWallDist;
					var currentFloorX: Float = weight * floorXWall + (1.0 - weight) * posX;
					var currentFloorY: Float = weight * floorYWall + (1.0 - weight) * posY;
					var TexX: Int = Math.floor(currentFloorX * floortex.width) % floortex.width;
					var TexY: Int = Math.floor(currentFloorY * floortex.height) % floortex.height;
					var pix: Int = floortex.getPixel(TexX, TexY);
					setPixel(sx, sy, pix);
					
				}
				
				
				//floor and ceiling will need a little math
			}
				
			//drawcolumn(sx, distance, texX);
		}
		
		unlock();
		
	}
	
	
	
	public function isWall(mx: Int, my: Int) : Int
	{
		if (mx < 0 || my < 0 || mx > (MAPWIDTH - 1) || my > (MAPHEIGHT - 1)) return -1;
		else return map[my * MAPWIDTH + mx];
	}
	
	

}